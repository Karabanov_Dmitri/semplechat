package ru.example.chat.setver;

import ru.example.network.TCPConnection;
import ru.example.network.TCPConnectionListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

/**
 * Created by karab on 10.02.2018.
 */
public class ChatServer implements TCPConnectionListener {
    public static void main(String[] args) {
        new ChatServer();
    }

    private final ArrayList<ru.example.network.TCPConnection> connections = new ArrayList<>();

    private ChatServer() {
        System.out.println("Server running ...");
        try (ServerSocket serverSocket = new ServerSocket(8189)) {
            while (true) {
                try {
                    new ru.example.network.TCPConnection(this, serverSocket.accept());
                } catch (IOException e) {
                    System.out.println("TCPConnection exception" + e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized void onConnectionReady(ru.example.network.TCPConnection tcpConnection) {
        connections.add(tcpConnection);
        sendToAllConnections("TCPConnection " + tcpConnection);
    }


    @Override
    public synchronized void onReceiveString(TCPConnection tcpConnection, String value) {
        sendToAllConnections(value);
    }

    @Override
    public synchronized void onDisconnect(ru.example.network.TCPConnection tcpConnection) {
        connections.remove(tcpConnection);
        sendToAllConnections("Client disconnected" + tcpConnection);
    }

    @Override
    public void onException(ru.example.network.TCPConnection tcpConnection, IOException e) {
        System.out.println("TCPConnection exception" + e);
    }

    private void sendToAllConnections(String value) {
        System.out.println(value);
        for (ru.example.network.TCPConnection connection : connections) {
            connection.sendString(value);
        }
    }
}
